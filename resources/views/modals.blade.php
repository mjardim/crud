<div id="cadastrarPessoa" class="modal fade bd-example-modal-lg modal-auxiliar" role="dialog">
	<div class="modal-dialog" role="document" style="max-width: 800px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Adicionar Pessoa</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				@include('pessoas.create')
			</div>
		</div>
	</div>
</div>

<div id="editarPessoa" class="modal fade bd-example-modal-lg modal-auxiliar" role="dialog">
	<div class="modal-dialog" role="document" style="max-width: 800px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Editar Pessoa</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			{{ Form::hidden('id', null) }}
			<div class="modal-body">
				@include('pessoas.edit')
			</div>
		</div>
	</div>
</div>