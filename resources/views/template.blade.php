<!DOCTYPE html>
<html lang="pt">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Teste</title>

	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('css/app_modal.css?time=' . date('His')) }}">

	<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/bootstrap.js') }}"></script>
	<script type="text/javascript" src="{{ asset('vendor/sweetalert/sweetalert.all.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/script.js?time=' . date('His')) }}"></script>


</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				@yield('content')
			</div>
		</div>
	</div>

	@include('modals')
	<script>
		var _token = '{{ csrf_token() }}';
	</script>
</body>
</html>