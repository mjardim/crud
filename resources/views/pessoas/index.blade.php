@extends('template')

@section('content')
<main>
    <div class="container-fluid p-0">

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div id="datatables-reponsive_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                            
                            <div class="row">
                                <div class="col-11">
                                </div>
                                <div class="col-1">
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-info btn-create">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="resultado-busca">
                                        <table id="datatables-reponsive" class="table table-striped dataTable no-footer dtr-inline collapsed table-hover" style="width: 100%;" role="grid" aria-describedby="datatables-reponsive_info">
                                            <thead>
                                                <tr role="row">
                                                    <th style="width: 40%;">Nome</th>
                                                    <th style="width: 15%;">Nascimento</th>
                                                    <th style="width: 15%;">Genero</th>
                                                    <th style="width: 20%;">País</th>
                                                    <th style="width: 5%;">Editar</th>
                                                    <th style="width: 5%;">Excluir</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($pessoas as $pessoa)
                                                    <tr>
                                                        <td>{{ $pessoa->nome }}</td>
                                                        <td>{{ $pessoa->nascimento }}</td>
                                                        <td>{{ $pessoa->genero }}</td>
                                                        <td>
                                                            {{ $pessoa->pais }}
                                                        </td>
                                                        <td>
                                                            <a href="#" data-edit-id="{{ $pessoa->id }}" type="button" class="btn btn-info btn-edit">
                                                                <i class="fa fa-edit"></i>
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="#" data-delete-id="{{ $pessoa->id }}" class="btn btn-danger btn-delete" title="Excluir">
                                                                <i class="fa fa-trash"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach

                                                @if ($pessoas->isEmpty())
                                                    <tr>
                                                        <td colspan="6" class="text-center">
                                                            <em>Nenhuma informação encontrada.</em>
                                                        </td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</main>
@endsection