<div class="content">
	
	<div class="form-group row">
		<div class="col-7">
			<label class="form-label">Nome *</label>
			{{ Form::text('nome', null, ['class' => 'form-control', 'required', 'autofocus', 'maxlength' => 50]) }}
			@include('error', ['field' => 'nome'])
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-3">
			<label class="form-label">Nascimento *</label>
			{{ Form::text('nascimento', null, ['class' => 'form-control data', 'maxlength' => '10']) }}
			@include('error', ['field' => 'nascimento'])
		</div>
	</div>

	<div class="form-group">
		<div class="col-5">
			<label class="form-label">Gênero *</label>
			{{ Form::select('genero', $tipoGenero, null, ['class' => 'form-control']) }}
			@include('error', ['field' => 'genero'])
		</div>
	</div>

	<div class="form-group">
		<div class="col-5">
			<label class="form-label">País *</label>
			{{ Form::select('pais_id', $paises, null, ['class' => 'form-control', 'required']) }}
			@include('error', ['field' => 'pais_id'])
		</div>
	</div>

	<div class="form-group row">
		<div class="col-sm-12">
			{{ Form::submit('Cadastrar', ['class' => 'btn btn-primary', 'id' => 'btn-create']) }}
		</div>
	</div>
</div>