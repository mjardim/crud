$(document).on('click', '.btn-create', function (e) {
    // Abrir o modal da pessoa
    $('#cadastrarPessoa').modal('show');
    $('#editarPessoa').hide();
    limparErros();
});

$(document).on('click', '#btn-create', function (e) {
    e.preventDefault();
    actionModal('cadastrarPessoa', 'POST');
});

$(document).on('click', '#btn-edit', function (e) {
    e.preventDefault();
    actionModal('editarPessoa', 'PUT');
});

function actionModal(modal, methodModal){
    var	nome = $('#'+modal+' input[name="nome"]').val();
    var	nascimento = $('#'+modal+' input[name="nascimento"]').val();
    var	genero = $('#'+modal+' select[name="genero"]').val();
    var	pais_id = $('#'+modal+' select[name="pais_id"]').val();

    var id = '';
    var urlModal = '/pessoas';
    if(methodModal == 'PUT'){
        id = $('#'+modal+' input[name="id"]').val();
        urlModal += '/'+id;
    }

    // Requisição ajax
    $.ajax({
        url: urlModal,
        method: methodModal, // POST, GET, PUT, DELETE,
        data: {
            nome: nome,
            nascimento: nascimento,
            genero: genero,
            pais_id: pais_id,
            _token: _token,
            id: id,
        },
        dataType: 'json',
        success: function(response) {

            //mostra mensagem
            Swal.fire({
                icon: response.icon,
                title: response.message,
                showConfirmButton: false,
                timer: 1500
            })

            //limpa os campos
            limparCampos(modal);
            window.location.reload();

        }, 
        error: function(response) {
            //limpa os erros
            limparErros(modal);

            //mostra os novos erros
            error = response.responseJSON.errors;
            $.each(error, function(key, value) {
                if(key == 'nome' || key == 'nascimento'){
                    $('#'+modal+' input[name="'+key+'"]').addClass('is-invalid');
                }else{
                    $('#'+modal+' select[name="'+key+'"]').addClass('is-invalid');
                }
                $('#'+modal+' #validation-'+key+'-error').html(value);
            });
        }
    });
}

$(document).on('click', '.btn-edit', function () {

    var	id = $(this).attr('data-edit-id');
    
    // Requisição ajax
    $.ajax({
        url: '/pessoas/'+id+'/edit',
        method: 'GET', // POST, GET, PUT, DELETE,
        data: {
            _token: _token,
        },
        dataType: 'json',
        success: function(response) {

            $('#editarPessoa').find('[name="nome"]').val(response.nome);
            $('#editarPessoa').find('[name="nascimento"]').val(response.nascimento);
            $('#editarPessoa').find('[name="genero"]').val(response.genero);
            $('#editarPessoa').find('[name="pais_id"]').val(response.pais_id);
            $('#editarPessoa').find('[name="id"]').val(response.id);

            // Abrir modal
            $('#editarPessoa').modal('show');
            $('#cadastrarPessoa').hide();
            limparErros('editarPessoa');
        }, 
        error: function(response) {

            Swal.fire({
                icon: 'error',
                title: response.message,
                showConfirmButton: false,
                timer: 2000
            })
        }
    });
});

$(document).on('click', '.btn-delete', function(e) {
    var	id = $(this).attr('data-delete-id'),
        token = $('[name="_token"]').val(),
            obj = $(this);

    Swal.fire({
        title: 'Tem certeza que deseja excluir?',
        text: "Você não poderá reverter isso!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'OK'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '/pessoas/'+ id,
                method: 'DELETE',
                data: {
                    id: id,
                    _token: _token,
                },
                success: function(response){
                    obj.parent().parent().remove();
                },
            });
        }
    })
});

// Adequando a exibição do backdrop dos modais
$('.modal').on('shown.bs.modal', function (e) {
    var indexModal = $('.modal.show').length;
    // Corrige a exibição do backgrop
    $(this).css('z-index', (100 * indexModal) + 1);
    $('.modal-backdrop.show').last().css('z-index', 100 * indexModal);
});

function limparCampos(modal){
    $(modal+' input, '+modal+' select').val(''); 
    limparErros(modal);
}

function limparErros(modal){
    // Limpa possíveis erros exibidos
    $('#'+modal+' input, #'+modal+' select').removeClass('is-invalid');
}

/*$('.data').datetimepicker({ 
    locale: 'pt-br', 
    format: 'DD/MM/YYYY', 
    disabledHours: false, 
});*/