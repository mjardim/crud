<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use App\Helpers\Formata;
use App\Models\Pessoa;
use App\Models\Pais;

use Illuminate\Http\Request;
use App\Http\Requests\PessoasPostRequest;

class PessoasController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

		$tipoGenero = [
			'M' => 'Masculino',
			'F' => 'Feminino',
			'' => 'Não informado',
		];

        $pessoas = Pessoa::all();
        $paises = Pais::asOptions();
		
		foreach ($pessoas as $pessoa) {
            $pessoa->nascimento = Formata::dataLegivel($pessoa->nascimento, true);
			$pessoa->genero = $tipoGenero[$pessoa->genero];
			$pessoa->pais = $pessoa->pais->nome;
		}

        return view('pessoas.index', compact('pessoas', 'tipoGenero', 'paises'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pessoas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PessoasPostRequest $request)
    {

        // Retorno negativo
		$response['icon'] = 'Error';
        $response['success'] = false;

        // Captura os dados do request
		$data = $request->all();
        
        // Cria um objeto pessoa
		$pessoa = new Pessoa;
		// Preenche com as informações obtidas
		$pessoa->fill($data);
        $pessoa->nascimento = Formata::dataBanco($pessoa->nascimento);

		// Inicia a transação com o banco de dados
		DB::beginTransaction();

		// Salva as informações inseridas no objeto
		$sucesso = $pessoa->save();
		// Verifica se tudo ocorreu corretamente
		if ($sucesso) {
			// Efetiva a atualização da base de dados
			DB::commit();

            // Retorno positivo
			$response['message'] = 'Sucesso';
            $response['icon'] = 'success';
			// Retorno ajax quando o cadastro acontece via modal
            $response['success'] = true;
		}else{
            $response['message'] = $data['errors'];
        }

		echo json_encode($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pessoa = Pessoa::find($id);
        $pessoa->nascimento = Formata::dataLegivel($pessoa->nascimento, true);
        $pessoa->genero = $pessoa->genero;
        $pessoa->pais = $pessoa->pais->nome;

        echo json_encode($pessoa);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PessoasPostRequest $request, $id)
    {
        $pessoa = Pessoa::find($id);
		$data = $request->all();

        $data['nascimento'] = Formata::dataBanco($data['nascimento']);
        $sucesso = $pessoa->update($data);

		if ($sucesso) {
			// Retorno positivo
			$response['message'] = 'Sucesso';
            $response['icon'] = 'success';
			// Retorno ajax quando o cadastro acontece via modal
            $response['success'] = true;
		}else{
            // Retorno negativo
		    $response['icon'] = 'Error';
            $response['success'] = false;
            $response['message'] = $data['errors'];
        }

        echo json_encode($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $pessoa = Pessoa::find($id);
		$data = $request->all();

		if ($pessoa->delete()) {
			$response['success'] = true;
		}else{
            $response['success'] = false;
        }

		echo json_encode($response);
    }
}
