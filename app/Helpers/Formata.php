<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

abstract class Formata {
	/**
	 * Transforma uma collection ou array em um array próprio para popular um SELECT
	 *
	 * @param  array/Collection $valores 	Os componentes que vão popular o select
	 * @param  string 			$campoId 	O index do array ou a propriade que vai 'value' do option
	 * @param  string 			$campoTexto O index do array ou a propriade que vai como texto do option
	 * @return array
	 */
	public static function options($valores, $campoId = 'id', $campoTexto = 'nome') {
		if (!is_array($valores)) {
			$valores = $valores->toArray();
		}

		$itens = [];
		foreach ($valores as $valor) {
			$itens[ $valor[$campoId] ] = $valor[$campoTexto];
		}

		return $itens;
	}

	/**
	 * Transforma a data para um formato legível
	 *
	 * @param  string $data
	 * @return string
	 */
	public static function dataLegivel($data, $ocultarHora = false) {
		// Já formatada
		if (empty($data) || false !== strpos($data, '/')) {
			return $data;
		}

		// Divide data e hora em vars
		@list($data, $hora) = explode(' ', $data);

		// Formata a data
		$data = date_create_from_format('Y-m-d', $data)->format('d/m/Y');

		// Existe hora? Adiciona à string de data
		if (!$ocultarHora && !empty($hora)) {
			$data = "{$data} " . substr($hora, 0, 5);
		}

		return $data;
	}

	/**
	 * Formata para padrão Y-m-d
	 *
	 * @param  string $data
	 * @return string
	 */
	public static function dataBanco($data) {

		if ($d = date_create_from_format('d/m/Y', $data)) {
			return $d->format('Y-m-d');
		}

		if($d = date_create_from_format('d/m/Y H:i', $data)) {
			return $d->format('Y-m-d H:i');
		}

		return $data;
	}

}
?>
