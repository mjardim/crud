<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class SBuilder extends Builder {

	/**
	 * Retorna somente a propriedade de cada objeto
	 *
	 * @param  string $propriedade
	 * @return array
	 */
	public function prop($propriedade) {
		return $this->get()->prop($propriedade);
	}

	/**
	 * Faz a collection ficar no formato passado
	 *
	 * @param  array $formato
	 * @return \App\SCollection
	 */
	public function formata(array $formato) {
		return $this->get()->formata($formato);
	}

	/**
	 * Devolve um array com as colunas para popular um <select>
	 *
	 * @param  string $id
	 * @param  string $value
	 * @return array
	 */
	public function asOptions($key = 'id', $value = 'nome') {
		$ordenado = !empty($this->getQuery()->orders);

		return $this
			->orderBy($value, 'asc')
			->get()
			->asOptions($key, $value);
	}
}
