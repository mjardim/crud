<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\BaseModel;
use App\Traits\TrataBuilder;
use App\Traits\TrataCollection;

class Pessoa extends BaseModel {
	use TrataCollection, TrataBuilder, HasFactory;

    public $table = 'pessoa';
	public $fillable = [
		'id', 'nome', 'nascimento', 'genero', 'pais_id',
	];
	public $searchable = [
		'id', 'nome', 'nascimento', 'genero', 'pais_id',
	];

	/**
	 * Retorna somente o fornecedor que tiver o nome exato
	 *
	 * @param  string $nome
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	public static function getByNome($nome) {
		return self
			::where('nome', $nome)
			->first();
	}

	/**
	 * Relacionamentos
	 */
	public function pais() {
		return $this->belongsTo(Pais::class);
	}

}

