<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;
use App\Traits\TrataBuilder;
use App\Traits\TrataCollection;

class Pais extends BaseModel {
	use TrataCollection, TrataBuilder;

    public $table = 'pais';
	public $fillable = ['codigo', 'nome'];
	public $searchable = ['codigo', 'nome'];

	/**
	 * Retorna somente o fornecedor que tiver o nome exato
	 *
	 * @param  string $nome
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	public static function getByPais($nome) {
		return self
			::where('nome', $nome)
			->first();
	}

}