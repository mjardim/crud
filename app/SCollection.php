<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class SCollection extends Collection {
	/**
	 * Retorna somente a propriedade de cada objeto
	 *
	 * @param  string $propriedade
	 * @return array
	 */
	public function prop($propriedade) {
		// SCollection vazia?
		if ($this->isEmpty()) {
			return [];
		}

		return $this->keyBy($propriedade)->keys()->toArray();
	}

	/**
	 * Faz a collection ficar no formato passado
	 *
	 * @param  array $formato
	 * @return \App\SCollection
	 */
	public function formata(array $formato) {
		// SCollection vazia?
		if ($this->isEmpty()) {
			return new SCollection;
		}

		// Chave da SCollection
		$propId = key($formato);
		// Conteúdo de cada array
		$props  = is_array($formato[$propId]) ? $formato[$propId] : [$formato[$propId]];

		return $this->keyBy($propId)
			->map(function($obj) use($props) {
				// Se for única, retorna sem array
				if (count($props) == 1) {
					return $obj->{end($props)};
				}

				// Propriedades a serem retornada em cada índice
				foreach ($props as &$prop) {
					$prop = $obj->{$prop};
				}

				return $props;
			});
	}

	/**
	 * Devolve um array para popular um <select>
	 *
	 * @param  string $key
	 * @param  string $value
	 * @return array
	 */
	public function asOptions($key = 'id', $value = 'nome') {
		return $this->formata([$key => $value])->toArray();
	}
}
