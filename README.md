<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## About

<b>CRUD</b> is an open source application built with Laravel to manage people.

In this project were developed:
- a grid to show registered people;
- option to create and edit by opening a modal
- option to exclude a person after confirming whether or not to exclude.
- validation of name, birth, gender and country fields.

## How to Install
- <code>Install Xampp</code>
- <code>criar db -> 'test'</code>
- <code>Copiar o arquivo .env.example -> .env</code>
- <code>No arquivo '.env'</code>
    - incluir no APP_URL a porta ':8000'
    - mudar o DB_DATABASE para 'test'
- <code>php artisan key:generate --ansi</code>
- <code>composer install</code>
- <code>php artisan migrate</code>
- <code>php artisan db:seed</code>
- <code>php artisan serve</code>
- Then Access: localhost:8000 or 127.0.0.1:8000