<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Pais as Pais;

class PaisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pais::create(
            ['id' => 1, 'nome' => 'Brasil'],
            ['id' => 2, 'nome' => 'United States of America'],
        );
    }
}
